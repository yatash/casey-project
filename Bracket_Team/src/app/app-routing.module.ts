import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{HomepageComponent}from'../app/homepage/homepage.component'
import { FeaturepageComponent } from './featurepage/featurepage.component';

const routes: Routes = [
   {
    path: '',
    pathMatch: 'full',
    redirectTo: 'Home'
   },
   {
     path:'Home',
     component:HomepageComponent
   },
   {
      path:'feature',
     component:FeaturepageComponent 
   }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
