import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturepageComponent } from './featurepage.component';

describe('FeaturepageComponent', () => {
  let component: FeaturepageComponent;
  let fixture: ComponentFixture<FeaturepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
